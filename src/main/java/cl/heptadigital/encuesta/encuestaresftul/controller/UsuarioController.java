 package cl.heptadigital.encuesta.encuestaresftul.controller;
 
 import java.util.Random;
 import org.apache.commons.codec.binary.Base64;
 import org.apache.commons.codec.digest.DigestUtils;
 import org.springframework.web.bind.annotation.RestController;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 @RestController
 public class UsuarioController
 {
   private static final Integer SIZE_SALT = Integer.valueOf(8);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
   public static String generarTextoRandom(int size) {
/* 113 */     int cantCaracteres = (int)(size * 0.75D);
     
/* 115 */     byte[] random = new byte[cantCaracteres];
     
/* 117 */     Random rand = new Random();
     
/* 119 */     rand.nextBytes(random);
     
/* 121 */     String result = Base64.encodeBase64String(random);
     
/* 123 */     if (result.length() != size) {
/* 124 */       String debugRandom = "[";
       
/* 126 */       for (byte digito : random) {
/* 127 */         debugRandom = debugRandom + Integer.valueOf(digito).toString() + ",";
       }
/* 129 */       debugRandom = debugRandom + "]";
     } 
 
     
/* 133 */     return result;
   }
 
 
 
 
 
   
   public static String encriptarClave(String clave) {
/* 142 */     String salt = generarTextoRandom(SIZE_SALT.intValue());
/* 143 */     return salt + DigestUtils.md5Hex(salt + clave);
   }
 }


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/controller/UsuarioController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */