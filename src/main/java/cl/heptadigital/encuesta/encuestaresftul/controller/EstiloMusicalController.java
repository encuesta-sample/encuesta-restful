package cl.heptadigital.encuesta.encuestaresftul.controller;

import cl.heptadigital.encuesta.encuestaresftul.domain.EstiloMusicalEntity;
import cl.heptadigital.encuesta.encuestaresftul.repository.EstiloMusicalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EstiloMusicalController
{
  @Autowired
  EstiloMusicalRepository repository;
  
  @RequestMapping(value = {"/estilomusical/list"}, method = {RequestMethod.GET})
  public ResponseEntity<Object> list() {
     Iterable<EstiloMusicalEntity> listaEstilosMusicales = this.repository.findAll();
     return new ResponseEntity(listaEstilosMusicales, HttpStatus.OK);
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/controller/EstiloMusicalController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */