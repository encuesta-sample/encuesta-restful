package cl.heptadigital.encuesta.encuestaresftul.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class MantenedorUsuariosController {
  private static final int PERMISO_USUARIOS = 1;
  
  private static final int DEFAULT_SIZE_PASSWORD = 4;
}
