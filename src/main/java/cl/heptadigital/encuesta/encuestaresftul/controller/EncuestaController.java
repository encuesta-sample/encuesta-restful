package cl.heptadigital.encuesta.encuestaresftul.controller;

import cl.heptadigital.encuesta.encuestaresftul.domain.EncuestaEntity;
import cl.heptadigital.encuesta.encuestaresftul.model.EncuestaDTO;
import cl.heptadigital.encuesta.encuestaresftul.repository.EncuestaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;





@RestController
public class EncuestaController
{
  @Autowired
  EncuestaRepository repository;
  
  @RequestMapping(value = {"/encuesta/create"}, method = {RequestMethod.POST})
  public ResponseEntity<Object> create(@RequestBody EncuestaDTO dto) {
     EncuestaEntity entity = new EncuestaEntity();
     entity.setEmail(dto.getEmail());
     entity.setEstiloMusicalCodigo(dto.getEstiloMusicalCodigo());
     this.repository.save(entity);
     return new ResponseEntity(HttpStatus.OK);
  }
  
  @RequestMapping(value = {"/encuesta/result"}, method = {RequestMethod.GET})
  public ResponseEntity<Object> list() {
     List<EncuestaRepository.EncuestaResult> resultEncuesta = this.repository.getResults();
     return new ResponseEntity(resultEncuesta, HttpStatus.OK);
  }
}