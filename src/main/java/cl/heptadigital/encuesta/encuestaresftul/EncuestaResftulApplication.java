package cl.heptadigital.encuesta.encuestaresftul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cl.heptadigital.encuesta.encuestaresftul.config.ApplicationProperties;

@SuppressWarnings("deprecation")
@ConfigurationPropertiesScan
@SpringBootApplication
public class EncuestaResftulApplication implements CommandLineRunner
{
	@Autowired
	ApplicationProperties properties;
  
  public static void main(String[] args) {
     SpringApplication.run(EncuestaResftulApplication.class, args);
  }
	
	@SuppressWarnings("deprecation")
	@Bean
 public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurerAdapter() {
       @Override
       public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping("/**").allowedOrigins(properties.getAllowCors()).allowedMethods("GET", "POST", "OPTIONS", "PUT", "DELETE" );
       }
    };
 }

@Override
public void run(String... args) throws Exception {
}
}
