package cl.heptadigital.encuesta.encuestaresftul.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * Clase que representa el archivo de configuración application.properties
 * Para que esta clase funcione se debe agregar la anotación @ConfigurationPropertiesScan a la clase @SpringBootApplication
 * Cada nueva propiedad se debe agregar al constructor. No se deben agregar setters a la clase para protegerla contra escritura
 * @author gorson
 *
 */

@ConstructorBinding
@ConfigurationProperties("application")
public class ApplicationProperties {
	private String allowCors;
	
	public ApplicationProperties(String allowCors) {
		super();
		this.allowCors = allowCors;
	}

	public String getAllowCors() {
		return allowCors;
	}
	
}
