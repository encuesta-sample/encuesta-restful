package cl.heptadigital.encuesta.encuestaresftul.model;





public class CodDescDTO
{
  private Integer cod;
  private String desc;
  
  public CodDescDTO() {}
  
  public CodDescDTO(Integer cod, String desc) {
     this.cod = cod;
     this.desc = desc;
  }

  
  public Integer getCod() {
     return this.cod;
  }

  
  public void setCod(Integer cod) {
     this.cod = cod;
  }

  
  public String getDesc() {
     return this.desc;
  }

  
  public void setDesc(String desc) {
     this.desc = desc;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/model/CodDescDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */