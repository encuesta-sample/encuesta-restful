package cl.heptadigital.encuesta.encuestaresftul.model;

public class EncuestaDTO {
  private String email;
  private Integer estiloMusicalCodigo;
  
  public String getEmail() {
/*  8 */     return this.email;
  }
  
  public void setEmail(String email) {
     this.email = email;
  }
  
  public Integer getEstiloMusicalCodigo() {
     return this.estiloMusicalCodigo;
  }
  
  public void setEstiloMusicalCodigo(Integer estiloMusicalCodigo) {
     this.estiloMusicalCodigo = estiloMusicalCodigo;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/model/EncuestaDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */