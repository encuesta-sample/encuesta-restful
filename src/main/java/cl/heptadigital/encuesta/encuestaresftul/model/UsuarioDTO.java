package cl.heptadigital.encuesta.encuestaresftul.model;


public class UsuarioDTO
{
  private Long id;
  private String username;
  private String nombres;
  private String apellidos;
  private String permisos;
  private String email;
  private String token;
  
  public UsuarioDTO() {}
  
  public UsuarioDTO(Long id, String username, String nombres, String apellidos, String permisos, String email) {
     this.id = id;
     this.username = username;
     this.nombres = nombres;
     this.apellidos = apellidos;
     this.permisos = permisos;
     this.email = email;
  }
  public Long getId() {
     return this.id;
  }
  public void setId(Long id) {
     this.id = id;
  }
  public String getUsername() {
     return this.username;
  }
  public void setUsername(String username) {
     this.username = username;
  }
  public String getNombres() {
     return this.nombres;
  }
  public void setNombres(String nombres) {
     this.nombres = nombres;
  }
  
  public String getApellidos() {
     return this.apellidos;
  }
  public void setApellidos(String apellidos) {
     this.apellidos = apellidos;
  }
  public String getPermisos() {
     return this.permisos;
  }
  public void setPermisos(String permisos) {
     this.permisos = permisos;
  }
  public String getEmail() {
     return this.email;
  }
  public void setEmail(String email) {
     this.email = email;
  }
  public String getToken() {
     return this.token;
  }
  public void setToken(String token) {
     this.token = token;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/model/UsuarioDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */