package cl.heptadigital.encuesta.encuestaresftul.repository;

import cl.heptadigital.encuesta.encuestaresftul.domain.EncuestaEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface EncuestaRepository extends CrudRepository<EncuestaEntity, String> {
  @Query(value = "SELECT \n\tESMU_CODIGO as esmuCodigo,\n\tESMU_DESCRIPCION as esmuDescripcion,\n\tISNULL(CANT,0) AS CANT\nFROM PUBLIC.ESTILO_MUSICAL EM\nLEFT JOIN\n(SELECT \n\tENCU_ESMU_CODI,\n\tcount(1) AS CANT \nFROM PUBLIC.ENCUESTA GROUP BY (ENCU_ESMU_CODI)) TOTAL\nON EM.ESMU_CODIGO =TOTAL.ENCU_ESMU_CODI", nativeQuery = true)
  List<EncuestaResult> getResults();
  
  public static interface EncuestaResult{
	  public Integer getEsmuCodigo();
	  public String getEsmuDescripcion();
	  public Integer getCant();
  }
}

