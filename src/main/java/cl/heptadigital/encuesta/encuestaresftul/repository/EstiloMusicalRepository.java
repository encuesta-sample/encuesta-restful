package cl.heptadigital.encuesta.encuestaresftul.repository;

import cl.heptadigital.encuesta.encuestaresftul.domain.EstiloMusicalEntity;
import org.springframework.data.repository.CrudRepository;

public interface EstiloMusicalRepository extends CrudRepository<EstiloMusicalEntity, Long> {}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/repository/EstiloMusicalRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */