package cl.heptadigital.encuesta.encuestaresftul.repository;

import cl.heptadigital.encuesta.encuestaresftul.domain.UsuarioEntity;

public interface UsuarioRepository {
  UsuarioEntity findByUsername(String paramString);
  
  UsuarioEntity findByEmail(String paramString);
}