package cl.heptadigital.encuesta.encuestaresftul.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class UsuarioEntity
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String username;
  private String password;
  private String nombres;
  private String apellidos;
  private String permisos;
  private String email;
  
  public Long getId() {
     return this.id;
  }
  public void setId(Long id) {
     this.id = id;
  }
  
  public String getUsername() {
     return this.username;
  }
  public void setUsername(String username) {
     this.username = username;
  }
  public String getPassword() {
     return this.password;
  }
  public void setPassword(String password) {
     this.password = password;
  }
  public String getNombres() {
     return this.nombres;
  }
  public void setNombres(String nombres) {
     this.nombres = nombres;
  }
  public String getApellidos() {
     return this.apellidos;
  }
  public void setApellidos(String apellidos) {
     this.apellidos = apellidos;
  }
  public String getPermisos() {
     return this.permisos;
  }
  public void setPermisos(String permisos) {
     this.permisos = permisos;
  }
  public String getEmail() {
     return this.email;
  }
  public void setEmail(String email) {
     this.email = email;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/domain/UsuarioEntity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */