package cl.heptadigital.encuesta.encuestaresftul.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "ESTILO_MUSICAL")
public class EstiloMusicalEntity
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ESMU_CODIGO")
  private Integer Codigo;
  @Column(name = "ESMU_DESCRIPCION")
  private String descripcion;
  
  public Integer getCodigo() {
     return this.Codigo;
  }
  
  public void setCodigo(Integer codigo) {
     this.Codigo = codigo;
  }
  
  public String getDescripcion() {
     return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
     this.descripcion = descripcion;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/domain/EstiloMusicalEntity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */