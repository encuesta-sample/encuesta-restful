package cl.heptadigital.encuesta.encuestaresftul.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "ENCUESTA")
public class EncuestaEntity
{
  @Id
  @Column(name = "ENCU_EMAIL")
  private String email;
  @Column(name = "ENCU_ESMU_CODI")
  private Integer estiloMusicalCodigo;
  
  public String getEmail() {
     return this.email;
  }
  
  public void setEmail(String email) {
     this.email = email;
  }
  
  public Integer getEstiloMusicalCodigo() {
     return this.estiloMusicalCodigo;
  }
  
  public void setEstiloMusicalCodigo(Integer estiloMusicalCodigo) {
     this.estiloMusicalCodigo = estiloMusicalCodigo;
  }
}


/* Location:              /home/gorson/workspaces/encuesta/target/encuesta-resftul-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes/cl/heptadigital/encuesta/encuestaresftul/domain/EncuestaEntity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */