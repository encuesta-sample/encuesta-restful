# encuesta-restful
Este proyecto es el backend construido sobre springboot microservicio y base de datos embebida

## Procedimiento de versionamiento
- Actualizar (pull)
- Bloquear archivo
- Hacer modificaciones
- Actualizar
- Commit
- Push
- Desbloquear archivo

## comandos git
- descargar el repositorio					`git clone https://gitlab.com/encuesta-sample/encuesta-restful.git`
- cambiarse de branch							`git checkout -b develop`
- actualizar desde gitlab						`git pull origin develop`
- subir cambios a gitlab						`git push -u origin develop`
- bloquear archivo `git lfs lock path/to/file.png`
- desbloquear archivo `git lfs unlock path/to/file.png`
- desbloquear archivo `git lfs unlock --id=123`
- ver archivos bloqueados `git lfs locks`

## Configuración equipo desarrollo
- Parámetro máquina virtual java `-Dlogging.level.NOMBREPAQUETE.NOMBREPAQUETE=TRACE`

## Docker
### Creación Imagen docker Spring boot

#### Opción 2: Local
- Detener el contenedor (si es que se está ejecutando) `docker container stop encuesta-restful`
- Eliminar el contenedor (si es que existe previamente) `docker container rm encuesta-restful`
- Eliminar la imagen (Si es que existe previamente) `docker rmi -f encuesta/restful`
- Abrir una consola dentro del proyecto, donde se ubica el archivo Dockerfile
- Construir Imagen `docker build -t encuesta/restful ./`
- Exportación imagen (Solo si es que se quiere subir la imagen a servidor sin integración continua) `docker save encuesta/restful > encuesta-restful.tar`

### Importación imagen docker Spring boot (Solo si es que no se ha construido localmente)

#### Opción 2: Local
- Abrir una terminal en la ruta donde se encuentre la imagen de docker `encuesta-restful`
- Importación imagen `docker load -i encuesta-restful.tar`

### Carga Inicial 
#### Desde Gitlab
##### Producción
##### Local desarrollo
- UNIX    `docker run -p 8080:8080 -e FILE_DATABASE=database/encuestadb -e ALLOW_CORS=http://localhost:4200 -it --name encuesta-restful encuesta/restful`

## Carga normal ( omitir si el docker esta ejecutandose)
- Arrancar `docker container start encuesta-restful`
- Acoplar ventana `docker attach encuesta-restful` Para ver log en tiempo real

## Check vitalidad del servicio
- http://localhost:8080/encuesta/result


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/encuesta-sample/encuesta-restful.git
git branch -M main
git push -uf origin main
```


## Links de interés
- [Editor de este archivo](https://remarkableapp.github.io/)
- [Modificar este archivo](https://docs.gitlab.com/ee/user/markdown.html)
