
#stage 1
FROM maven as build
WORKDIR /app
COPY . .
RUN ls
RUN ls database/
RUN mvn clean package
#stage 2
FROM openjdk:14-jdk-alpine
COPY --from=build /app/target/*.jar app.jar
RUN mkdir database
COPY --from=build /app/database/ database/
RUN ls
RUN ls database/
ENTRYPOINT ["java","-jar","/app.jar"] 
